package pl.edu.agh.legalprompter.models;


public class DescribedLegalFormula extends LegalFormula {

    protected String signature;

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return getConclusions() + " ("+getSignature()+")";
    }
}
