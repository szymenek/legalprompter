package pl.edu.agh.legalprompter.models;

import java.util.ArrayList;
import java.util.List;

public class LegalFormula implements Comparable<LegalFormula> {
	
	protected List<LegalFact> conclusions = new ArrayList<LegalFact>();
	protected List<LegalFact> premises = new ArrayList<LegalFact>();

	protected int initRank = 0;

	public LegalFormula() {
	}

	public LegalFormula(LegalFormula formula) {
		setConclusions(formula.getConclusions());
		setPremises(formula.getPremises());
	}
	
	public List<LegalFact> getConclusions() {
		return this.conclusions;
	}

	public void setConclusions(List<LegalFact> conclusions) {
		this.conclusions = conclusions;
	}

	public List<LegalFact> getPremises() {
		return this.premises;
	}

	public void setPremises(List<LegalFact> premises) {
		this.premises = premises;
	}

	public void addPremises(List<LegalFact> premises) {
		this.premises.addAll(premises);
	}

	public void setInitRank(int initRank) {
		this.initRank = initRank;
	}

	public String toString() {
		return getConclusions() + ": " + getPremises();
	}

	public int getRank() {
		int rank = initRank;
		if (premises != null) {
			for (LegalFact premise : premises) {
				if (premise != null)
					rank += premise.getRank();
			}
		}
		return rank;
	}

	public int compareTo(LegalFormula o) {
		return this.getRank() - o.getRank();
	}
}
