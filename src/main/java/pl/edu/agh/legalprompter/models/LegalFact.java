package pl.edu.agh.legalprompter.models;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class LegalFact {

    protected String fact;
    protected boolean status;
    protected List<List<LegalFact>> premises = new ArrayList<List<LegalFact>>();

    public LegalFact() {
        this.status = true;
        this.premises = new ArrayList<List<LegalFact>>();
    }

    public LegalFact(String fact) {
        setFact(fact);
        if(fact.startsWith("!"))
            this.status = false;
        else
            this.status = true;
        this.premises = new ArrayList<List<LegalFact>>();
    }

    public LegalFact(LegalFact legalFact) {
        setFact(legalFact.getFact());

        if(legalFact.getFact().startsWith("!")) {
            if(legalFact.getStatus()) {
                String info = "Inconsistent fact name and status:\nfact name: "
                        + legalFact.getFact() + "\nstatus: " + legalFact.getStatus()
                        + "\nRemove \"!\" or set status=false";
                throw new IllegalArgumentException(info);
            }
            this.status = false;
        }
        else {
            this.status = legalFact.getStatus();
        }

        if(legalFact.getPremises() != null)
            this.premises = legalFact.getPremises();
        else
            this.premises = new ArrayList<List<LegalFact>>();
    }

    public String getFact() {
        return this.fact;
    }

    public void setFact(String fact) {
        this.fact = fact.replace("!", "");
    }

    public boolean getStatus() {
        return this.status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        if(getStatus())
            return this.fact;
        else
            return "!"+this.fact;
    }

    public List<List<LegalFact>> getPremises() {
        return this.premises;
    }

    public void setPremises(List<List<LegalFact>> premises) {
        this.premises = premises;
    }

    public boolean addPremises(List<LegalFact> premises) {
        if(this.premises == null || premises == null)
            return false;

        Set<Object> setOfNewPremises = new HashSet<Object>();
        setOfNewPremises.addAll(premises);
        for(List<LegalFact> listOfLegalFacts : this.premises) {
            if(listOfLegalFacts != null) {
                Set<Object> setOfPremises = new HashSet<Object>();
                setOfPremises.addAll(listOfLegalFacts);
                if(setOfPremises.equals(setOfNewPremises))
                    return false;
            }
        }

        this.premises.add(premises);
        return true;
    }

    public boolean havePremises(List<LegalFact> premises) {
        if(this.premises == null || premises == null)
            return true;

        Set<Object> setOfNewPremises = new HashSet<Object>();
        setOfNewPremises.addAll(premises);
        for(List<LegalFact> listOfLegalFacts : this.premises) {
            if(listOfLegalFacts != null) {
                Set<Object> setOfPremises = new HashSet<Object>();
                setOfPremises.addAll(listOfLegalFacts);
                if(setOfPremises.equals(setOfNewPremises))
                    return true;
            }
        }

        return false;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj instanceof LegalFact) {
            LegalFact legalFact = (LegalFact) obj;
            return toString().equals(legalFact.toString());
        }
        return false;
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    public int getRank() {
        int rank = 0;
        if (premises != null && premises.size() > 0) {
            LegalFact fact = premises.get(0).get(0);
            rank += 1 + (fact != null ? fact.getRank() : 0);
        }
        return rank;
    }
}
