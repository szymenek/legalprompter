package pl.edu.agh.legalprompter.models;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class PrecedenceLegalFormula extends DescribedLegalFormula {

    protected Date date;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        return getConclusions() + " ("+getSignature()+" [" + formatter.format(getDate()) + "])";
    }
}
