package pl.edu.agh.legalprompter.utils;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import pl.edu.agh.legalprompter.models.LegalFact;

import java.util.Date;


public class LegalJsonImporter {

    private static Gson gson = null;

    protected LegalJsonImporter() {
    }

    public static Gson getInstance() {
        if(gson == null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(LegalFact.class, new LegalFactDeserializer());
            gsonBuilder.registerTypeAdapter(LegalFact.class, new LegalFactSerializer());
            gsonBuilder.registerTypeAdapter(Date.class, new DateDeserializer());
            gson = gsonBuilder.create();
        }
        return gson;
    }
}
