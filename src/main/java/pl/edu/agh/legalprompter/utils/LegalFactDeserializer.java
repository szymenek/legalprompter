package pl.edu.agh.legalprompter.utils;


import com.google.gson.*;
import pl.edu.agh.legalprompter.models.LegalFact;

import java.lang.reflect.Type;

public class LegalFactDeserializer implements JsonDeserializer<LegalFact> {

    public LegalFact deserialize(JsonElement json, Type type,
                                 JsonDeserializationContext context) throws JsonParseException
    {
        JsonObject jsonObject = (JsonObject) json;
        return new LegalFact(jsonObject.get("fact").getAsString());
    }
}
