package pl.edu.agh.legalprompter.utils;


import com.google.gson.*;
import pl.edu.agh.legalprompter.models.LegalFact;

import java.lang.reflect.Type;

public class LegalFactSerializer implements JsonSerializer<LegalFact> {

    public JsonElement serialize(LegalFact src, Type typeOfSrc, JsonSerializationContext context) {
        final JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("fact", src.toString());
        return jsonObject;
    }
}
