package pl.edu.agh.legalprompter.utils;


import com.google.gson.*;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateDeserializer implements JsonDeserializer<Date> {

    public Date deserialize(JsonElement json, Type type,
                                 JsonDeserializationContext context) throws JsonParseException
    {
        DateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        try {
            return formatter.parse(json.getAsString());
        } catch (ParseException e) {
            return null;
        }
    }
}
