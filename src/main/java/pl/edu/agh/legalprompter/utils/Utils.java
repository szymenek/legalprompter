package pl.edu.agh.legalprompter.utils;

import org.drools.core.spi.KnowledgeHelper;
import pl.edu.agh.legalprompter.core.Blackboard;
import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.models.LegalFormula;

import java.util.*;

public class Utils {

    public static final String[] WANTED_RESULT_OPTIONS = new String[] {"all", "innocent", "discontinuance", "guilty"};

    public static void editKnowledge(KnowledgeHelper kh, List<LegalFact> factsList,
                                     List<LegalFact> premises, List<LegalFact> newFacts) {
        for(LegalFact newFact : newFacts) {
            if(factsList.contains(newFact)) {
                for(LegalFact legalFact : factsList) {
                    if (legalFact.equals(newFact)) {
                        if(legalFact.addPremises(premises))
                            System.out.println(legalFact + " (" + legalFact.getPremises() + ")");
                    }
                }
            } else {
                newFact.addPremises(premises);
                kh.insert(newFact);
                System.out.println(newFact + " (" + newFact.getPremises() + ")");
            }
        }
    }

    public static boolean canExtendPremises(List<LegalFact> factsList, LegalFact newFact,
                                            List<LegalFact> premises) {
        if(factsList.contains(newFact)) {
            for (LegalFact legalFact : factsList) {
                if (legalFact.equals(newFact)) {
                    return !legalFact.havePremises(premises);
                }
            }
        }

        return false;
    }

    public static boolean checkIfUnknownFact(List<LegalFact> listOfFacts, String fact) {
        LegalFact factToCheck = new LegalFact(fact);
        LegalFact negFactToCheck = new LegalFact(fact);
        negFactToCheck.setStatus(!factToCheck.getStatus());
        if (listOfFacts.contains(factToCheck) || listOfFacts.contains(negFactToCheck)) {
            return false;
        }

        return true;
    }

    public static void sortTips(Blackboard blackboard) {
        List<LegalFormula> tips = blackboard.getTipFormulasNoVerbose();
        Collections.sort(tips);
        Map<LegalFormula, List<LegalFact>> map = new LinkedHashMap<LegalFormula, List<LegalFact>>();
        for (LegalFormula formula : tips) {
            map.put(formula, blackboard.getResultTips().get(formula));
        }

        blackboard.setResultTips(map);
    }


    public static void printTimeSummary(long start_time, long end_time) {
        System.out.println("Time: " + new Date().toString());
        double difference = (end_time - start_time)/1e6;
        int min = (int)(difference/60e3);
        int sec = (int)(difference-min*60e3)/1000;
        int millis = (int)(difference-min*60e3-sec*1e3);
        System.out.println("Difference: " + min + "m " + sec + "s " + millis + "ms");
    }
}
