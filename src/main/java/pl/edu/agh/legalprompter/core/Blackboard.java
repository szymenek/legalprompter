package pl.edu.agh.legalprompter.core;

import java.util.*;

import org.kie.api.runtime.KieSession;
import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.models.LegalFormula;
import pl.edu.agh.legalprompter.utils.Utils;

public class Blackboard {

	protected KieSession session;
	protected List<LegalFormula> resultPrecedences;
	protected List<LegalFormula> resultStatutes;
	Map<LegalFormula, List<LegalFact>> resultTips = new LinkedHashMap<LegalFormula, List<LegalFact>>();

	public Blackboard() {
		clearResults();
	}
	
	public KieSession getSession() {
		return session;
	}

	public void setSession(KieSession session) {
		this.session = session;
	}

	public void initializeFacts(List<LegalFact> legalFacts) {
		if(legalFacts != null && legalFacts.size() > 0) {
			for(LegalFact legalFact : legalFacts) {
				session.insert(legalFact);
			}
		}
	}
	
	public void saveResultPrecedence(LegalFormula precedence, List<LegalFact> premises) {
		precedence.setPremises(premises);
		if(!resultPrecedences.contains(precedence)) {
			resultPrecedences.add(precedence);
		}
	}

	public List<LegalFormula> getResultPrecedences(String wantedResult) {
		LegalFact wantedResultFact = new LegalFact(wantedResult);
		List<LegalFormula> res = new ArrayList<LegalFormula>();
		System.out.println("\nRESULTS (P):");
		for (LegalFormula formula : resultPrecedences) {
			if (Utils.WANTED_RESULT_OPTIONS[0].equals(wantedResult)
					|| formula.getConclusions().contains(wantedResultFact)) {
				res.add(formula);
			}
			System.out.println(formula);
		}
		return res;
	}

	public void saveResultStatutes(LegalFormula statute, List<LegalFact> premises) {
		statute.setPremises(premises);
		if(!resultStatutes.contains(statute)) {
			resultStatutes.add(statute);
		}
	}

	public List<LegalFormula> getResultStatutes() {
		System.out.println("\nRESULTS (S):");
		for(LegalFormula formula : resultStatutes) {
			System.out.println(formula);
		}
		return resultStatutes;
	}

	public void addResultTip(LegalFormula tip, List<LegalFact> unknownPremises) {
		resultTips.put(tip, unknownPremises);
	}

	public Map<LegalFormula, List<LegalFact>> getResultTips() {
		return resultTips;
	}

	public void setResultTips(Map<LegalFormula, List<LegalFact>> resultTips) {
		this.resultTips = resultTips;
	}

	public List<LegalFormula> getTipFormulas() {
		System.out.println("\nRESULTS (T):");
		for (LegalFormula formula : resultTips.keySet()) {
			System.out.println(formula);
		}
		return getTipFormulasNoVerbose();
	}

	public List<LegalFormula> getTipFormulasNoVerbose() {
		List<LegalFormula> resultTips = new ArrayList<LegalFormula>();
		for(LegalFormula formula : this.resultTips.keySet()) {
			resultTips.add(formula);
		}
		return resultTips;
	}

	public List<List<LegalFact>> getResultTipUnknownPremises() {
		List<List<LegalFact>> resultTipUnknownPremises = new ArrayList<List<LegalFact>>();
		for(List<LegalFact> unknownPremises : resultTips.values()) {
			resultTipUnknownPremises.add(unknownPremises);
		}
		return resultTipUnknownPremises;
	}

	public void clearResults() {
		resultPrecedences = new ArrayList<LegalFormula>();
		resultStatutes = new ArrayList<LegalFormula>();
		resultTips = new LinkedHashMap<LegalFormula, List<LegalFact>>();
	}
}
