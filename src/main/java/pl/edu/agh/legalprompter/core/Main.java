package pl.edu.agh.legalprompter.core;

import pl.edu.agh.legalprompter.agents.CommonKnowledgeAgent;
import pl.edu.agh.legalprompter.agents.GeneralLawAgent;
import pl.edu.agh.legalprompter.agents.PrecedenceAgent;
import pl.edu.agh.legalprompter.agents.StatuteAgent;
import pl.edu.agh.legalprompter.views.MainWindow;

import javax.swing.*;

public class Main {

	public static void main(String[] args) {

		QueueManager queueManager = new QueueManager(new Blackboard(), "current_case.json");
		queueManager.addAgent(new PrecedenceAgent("precedences.json"));
		queueManager.addAgent(new StatuteAgent("statutes.json"));
		queueManager.addAgent(new CommonKnowledgeAgent("common_knowledge.json"));
		queueManager.addAgent(new GeneralLawAgent("general_law.json"));

		runGui(queueManager);
//		runConsole(queueManager);
	}

	public static void runGui(final QueueManager queueManager) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new MainWindow(queueManager);
			}
		});
	}

	public static void runConsole(QueueManager queueManager) {
		queueManager.acquireKnowledge();
		queueManager.startLegalAnalysis();

		queueManager.getResultPrecedences();
		queueManager.getResultStatutes();
		queueManager.getTipFormulas();
	}
}
