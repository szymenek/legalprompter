package pl.edu.agh.legalprompter.core;

import java.io.*;
import java.util.*;

import org.kie.api.KieServices;
import org.kie.api.builder.KieBuilder;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.Message;
import org.kie.api.builder.Results;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import pl.edu.agh.legalprompter.agents.*;
import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.models.LegalFormula;
import pl.edu.agh.legalprompter.utils.Utils;

public class QueueManager {
	
	private Blackboard blackboard;
	private List<AbstractAgent> agents;
	private int tipEpsilon = 2;
	private String wantedResult = Utils.WANTED_RESULT_OPTIONS[0];
	private String currentCaseSourceFile = "current_case.json";

	public QueueManager(Blackboard blackboard, String currentCaseSourceFile) {
		this.blackboard = blackboard;
		this.agents = new ArrayList<AbstractAgent>();
		this.currentCaseSourceFile = currentCaseSourceFile;
	}
	
	public QueueManager(Blackboard blackboard, List<AbstractAgent> agentsList, String currentCaseSourceFile) {
		this.blackboard = blackboard;
		for(AbstractAgent agent : agentsList) {
			addAgent(agent);
		}
		this.currentCaseSourceFile = currentCaseSourceFile;
	}

	public Blackboard getBlackboard() {
		return this.blackboard;
	}

	public int getTipEpsilon() {
		return tipEpsilon;
	}

	public void setTipEpsilon(int value) {
		tipEpsilon = value;
	}

	public String getWantedResult() {
		return wantedResult;
	}

	public void setWantedResult(String value) {
		wantedResult = value;
	}
	
	public void addAgent(AbstractAgent agent) {
		this.agents.add(agent);
		agent.setBlackboard(this.blackboard);
	}
	
	public void acquireKnowledge() {
		for (AbstractAgent agent : this.agents)
			agent.acquireKnowledge();
	}
	
	public void startLegalAnalysis() {
		this.blackboard.clearResults();

		long start_time = System.nanoTime();
		System.out.println("\n\nTime: " + new Date().toString());

//		KnowledgeBuilder kbuilder = KnowledgeBuilderFactory.newKnowledgeBuilder();
//		kbuilder.add(ResourceFactory.newClassPathResource("LegalPrompter.drl"), ResourceType.DRL);
//		KieContainer kc = KieServices.Factory.get().getKieClasspathContainer();
//		KieSession session = kc.newKieSession("LegalPrompterKS");

		KieSession session = build();
		this.blackboard.setSession(session);

		initializeBlackboardFacts();

		for(AbstractAgent agent : this.agents) {
			session.insert(agent);
		}

		session.setGlobal("tipEpsilon", tipEpsilon);
		session.fireAllRules();
		session.dispose();

		long end_time = System.nanoTime();
		Utils.printTimeSummary(start_time, end_time);
	}

	private KieSession build() {
		KieServices kieServices = KieServices.Factory.get();
		KieFileSystem kfs = kieServices.newKieFileSystem();

		try {
			File tmpFile = File.createTempFile("lp_tmp",".drl");
			tmpFile.deleteOnExit();
			OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(tmpFile));
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream("LegalPrompter.drl");
			int in;
			while (-1 != (in = inputStream.read())){
				outputStream.write((byte) in);
			}
			outputStream.flush();
			outputStream.close();

			kfs.write("src/main/resources/LegalPrompter.drl",
					kieServices.getResources().newInputStreamResource(new FileInputStream(tmpFile)));

			KieBuilder kieBuilder = kieServices.newKieBuilder(kfs).buildAll();
			Results results = kieBuilder.getResults();
			if(results.hasMessages(Message.Level.ERROR)){
				System.out.println(results.getMessages());
				throw new IllegalStateException("___ERRORS___");
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		KieContainer kieContainer =
				kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());

//		KieBase kieBase = kieContainer.getKieBase();
		KieSession session = kieContainer.newKieSession();
//		session.addEventListener( new DebugAgendaEventListener() );
//		session.addEventListener( new DebugRuleRuntimeEventListener() );

		return session;
	}

	public List<LegalFact> getUnknownFacts() {
		List<LegalFact> currentCaseFacts = getCurrentCaseFacts();
		acquireKnowledge();
		List<LegalFact> uniqueFacts = new ArrayList<LegalFact>();
		for(AbstractAgent agent : agents) {
			for(LegalFormula legalFormula : agent.getKnowledge()) {
				for(LegalFact premise : legalFormula.getPremises()) {
					if(Utils.checkIfUnknownFact(uniqueFacts, premise.getFact()))
						uniqueFacts.add(premise);
				}
				if(!(agent instanceof PrecedenceAgent) && !(agent instanceof StatuteAgent)) {
					for(LegalFact conclusion : legalFormula.getConclusions()) {
						if(Utils.checkIfUnknownFact(uniqueFacts, conclusion.getFact()))
							uniqueFacts.add(conclusion);
					}
				}
			}
		}
		for(LegalFact currentFact : currentCaseFacts) {
			if(!Utils.checkIfUnknownFact(uniqueFacts, currentFact.getFact())) {
				uniqueFacts.remove(currentFact);
				LegalFact negCurrentFact = new LegalFact(currentFact);
				negCurrentFact.setStatus(!currentFact.getStatus());
				uniqueFacts.remove(negCurrentFact);
			}
		}
		return uniqueFacts;
	}

	public List<LegalFact> getCurrentCaseFacts() {
		CurrentCaseLoader currentCaseLoader = new CurrentCaseLoader(currentCaseSourceFile);
		currentCaseLoader.acquireKnowledge();
		LegalFormula legalFormula = currentCaseLoader.getKnowledge().get(0);
		if(legalFormula != null)
			return legalFormula.getPremises();

		return null;
	}

	public void dumpCurrentKnowledge(List<LegalFact> currentCaseFacts) {
		CurrentCaseLoader currentCaseLoader = new CurrentCaseLoader(currentCaseSourceFile);
		List<LegalFact> conclusions = new ArrayList<LegalFact>();
		conclusions.add(new LegalFact(""));
		LegalFormula formula = new LegalFormula();
		formula.setPremises(currentCaseFacts);
		formula.setConclusions(conclusions);
		List<LegalFormula> formulasList = new ArrayList<LegalFormula>();
		formulasList.add(formula);
		currentCaseLoader.setKnowledge(formulasList);
		currentCaseLoader.dumpKnowledge();
	}

	public List<LegalFormula> getResultStatutes() {
		return this.blackboard.getResultStatutes();
	}

	public List<LegalFormula> getResultPrecedences() {
		return this.blackboard.getResultPrecedences(wantedResult);
	}

	public List<LegalFormula> getTipFormulas() {
		return this.blackboard.getTipFormulas();
	}

	public List<List<LegalFact>> getResultTipUnknownPremises() {
		return this.blackboard.getResultTipUnknownPremises();
	}

	public Map<LegalFormula, List<LegalFact>> getResultTips() {
		return this.blackboard.getResultTips();
	}

	private void initializeBlackboardFacts() {
		List<LegalFact> legalFacts = getCurrentCaseFacts();

		if(legalFacts != null) {
			for (LegalFact legalFact : legalFacts) {
				legalFact.setPremises(null);
			}

			this.blackboard.initializeFacts(legalFacts);
		}
	}
}
