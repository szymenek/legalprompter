package pl.edu.agh.legalprompter.views.helpers;


import pl.edu.agh.legalprompter.models.LegalFact;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class KnowledgeTableModel extends AbstractTableModel {

    private static final long serialVersionUID = 1L;

    private List<String> factNames = new ArrayList<String>();
    private List<FactState> states = new ArrayList<FactState>();
    private String[] columnNamesList = {"Fact", "True", "False", "Unknown"};

    public KnowledgeTableModel(List<LegalFact> currentFacts, List<LegalFact> unknownFacts) {
        for(LegalFact fact : currentFacts) {
            factNames.add(fact.getFact());
            if(fact.getStatus())
                states.add(FactState.True);
            else
                states.add(FactState.False);
        }

        for(LegalFact fact : unknownFacts) {
            factNames.add(fact.getFact());
            states.add(FactState.Unknown);
        }
    }

    @Override
    public String getColumnName(int column) {
        return columnNamesList[column];
    }

    public int getRowCount() {
        return factNames.size();
    }

    public int getColumnCount() {
        return columnNamesList.length;
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        return ((columnIndex == 0) ? String.class : Boolean.class);
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex != 0);
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                factNames.set(rowIndex, (String)value);
                break;
            case 1:
                states.set(rowIndex, FactState.True);
                fireTableRowsUpdated(0, getRowCount() - 1);
                break;
            case 2:
                states.set(rowIndex, FactState.False);;
                fireTableRowsUpdated(0, getRowCount() - 1);
                break;
            case 3:
                states.set(rowIndex, FactState.Unknown);
                fireTableRowsUpdated(0, getRowCount() - 1);
                break;
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        switch(columnIndex){
            case 0:
                return factNames.get(rowIndex);
            case 1:
                return states.get(rowIndex) == FactState.True;
            case 2:
                return states.get(rowIndex) == FactState.False;
            case 3:
                return states.get(rowIndex) == FactState.Unknown;
            default:
                return null;
        }
    }

    public List<LegalFact> getCurrentCaseFacts() {
        List<LegalFact> currentCaseFacts = new ArrayList<LegalFact>();
        for(int i=0; i<states.size(); i++) {
            if(states.get(i) != FactState.Unknown) {
                LegalFact fact = new LegalFact(factNames.get(i));
                if(states.get(i) == FactState.True)
                    fact.setStatus(true);
                else
                    fact.setStatus(false);
                currentCaseFacts.add(fact);
            }
        }
        return currentCaseFacts;
    }

    public void clearFactsListStates() {
        for(int i=0; i<states.size(); i++)
            states.set(i, FactState.Unknown);
        fireTableRowsUpdated(0, getRowCount() - 1);
    }
}
