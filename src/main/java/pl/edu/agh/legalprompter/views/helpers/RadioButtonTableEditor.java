package pl.edu.agh.legalprompter.views.helpers;


import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class RadioButtonTableEditor extends DefaultCellEditor implements ItemListener {

    private JRadioButton radioButton = new JRadioButton();

    public RadioButtonTableEditor(JCheckBox checkBox) {
        super(checkBox);
    }

    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected,
                                                 int row, int column) {
        if (value == null)
            return null;

        radioButton.addItemListener(this);

        if (((Boolean) value))
            radioButton.setSelected(true);
        else
            radioButton.setSelected(false);

        return radioButton;
    }

    public Object getCellEditorValue() {
        return radioButton.isSelected();
    }

    public void itemStateChanged(ItemEvent e) {
        super.fireEditingStopped();
    }
}
