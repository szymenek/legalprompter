package pl.edu.agh.legalprompter.views.helpers;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class RadioButtonTableRenderer implements TableCellRenderer {

    private JRadioButton radioButton = new JRadioButton();

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected,
                                                   boolean hasFocus, int row, int column) {
        if (value == null)
            return null;

        radioButton.setSelected((Boolean) value);

        return radioButton;
    }
}
