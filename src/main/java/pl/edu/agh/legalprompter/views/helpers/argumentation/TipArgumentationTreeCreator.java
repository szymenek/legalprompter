package pl.edu.agh.legalprompter.views.helpers.argumentation;


import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.models.LegalFormula;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.*;

public class TipArgumentationTreeCreator extends ArgumentationTreeCreator {

    Map<LegalFormula, List<LegalFact>> resultTips = new LinkedHashMap<LegalFormula, List<LegalFact>>();

    public TipArgumentationTreeCreator(JTree tree, DefaultMutableTreeNode root, List<LegalFormula> formulas, Map<LegalFormula, List<LegalFact>> resultTips) {
        this.tree = tree;
        this.root = root;
        this.formulas = formulas;
        this.resultTips = resultTips;
    }

    @Override
    protected void createNode() {
        for(int i=0; i<formulas.size(); i++) {
            LegalFormula tipResult = formulas.get(i);
            DefaultMutableTreeNode conclusion = new DefaultMutableTreeNode(tipResult);
            root.add(conclusion);

            createNodeArgumentation(tipResult, conclusion, tipResult.getPremises());
        }
    }

    private void createNodeArgumentation(LegalFormula tipResult, DefaultMutableTreeNode node, List<LegalFact> facts) {
        for(LegalFact fact : facts) {
            String unknownFact = String.format("<html><font color=\"red\">%s</font></html>", fact);
            DefaultMutableTreeNode premise = new DefaultMutableTreeNode(resultTips.get(tipResult).contains(fact) ? unknownFact : fact);
            node.add(premise);
            if(fact.getPremises() != null && fact.getPremises().size() > 0)
                createNodeArgumentation(tipResult, premise, fact.getPremises().get(0));
        }
    }
}
