package pl.edu.agh.legalprompter.views;

import pl.edu.agh.legalprompter.core.QueueManager;
import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.utils.Utils;
import pl.edu.agh.legalprompter.views.helpers.RadioButtonTableEditor;
import pl.edu.agh.legalprompter.views.helpers.RadioButtonTableRenderer;
import pl.edu.agh.legalprompter.views.helpers.KnowledgeTableModel;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class KnowledgePanel extends JPanel {

    public KnowledgePanel(JFrame mainWindow, QueueManager queueManager) {

        setLayout(new BorderLayout());
        KnowledgeTableModel knowledgeTableModel = new KnowledgeTableModel(
                queueManager.getCurrentCaseFacts(), queueManager.getUnknownFacts());

        initTable(knowledgeTableModel);
        initOptions(mainWindow, knowledgeTableModel, queueManager);

        setVisible(true);
    }

    private void initTable(KnowledgeTableModel knowledgeTableModel) {
        JTable knowledgeTable = new JTable(knowledgeTableModel);

        final Font font = new Font("Arial", Font.PLAIN, 14);
        knowledgeTable.setFont(font);

        knowledgeTable.getColumnModel().getColumn(0).setMinWidth(300);
        knowledgeTable.getColumnModel().getColumn(1).setMaxWidth(45);
        knowledgeTable.getColumnModel().getColumn(2).setMaxWidth(45);
        knowledgeTable.getColumnModel().getColumn(3).setMaxWidth(60);

        knowledgeTable.getColumnModel().getColumn(1).setCellRenderer(new RadioButtonTableRenderer());
        knowledgeTable.getColumnModel().getColumn(1).setCellEditor(new RadioButtonTableEditor(new JCheckBox()));

        knowledgeTable.getColumnModel().getColumn(2).setCellRenderer(new RadioButtonTableRenderer());
        knowledgeTable.getColumnModel().getColumn(2).setCellEditor(new RadioButtonTableEditor(new JCheckBox()));

        knowledgeTable.getColumnModel().getColumn(3).setCellRenderer(new RadioButtonTableRenderer());
        knowledgeTable.getColumnModel().getColumn(3).setCellEditor(new RadioButtonTableEditor(new JCheckBox()));

        knowledgeTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

        JPanel panel = new JPanel();
        panel.add(new JScrollPane(knowledgeTable, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED));
        add(panel, BorderLayout.WEST);
    }

    private void initStartButton(final JFrame mainWindow, JPanel panel,
                                 final KnowledgeTableModel knowledgeTableModel, final QueueManager queueManager) {
        JButton btnApply = new JButton("Start");
        btnApply.setBounds(140, 100, 100, 40);
        panel.add(btnApply);

        btnApply.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                List<LegalFact> currentCaseFacts = knowledgeTableModel.getCurrentCaseFacts();
                setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                queueManager.dumpCurrentKnowledge(currentCaseFacts);
                queueManager.acquireKnowledge();
                queueManager.startLegalAnalysis();
                setCursor(Cursor.getDefaultCursor());
                ((MainWindow)mainWindow).updateGuiResults();
            }
        });
    }

    private void initClearButton(JPanel panel, final KnowledgeTableModel knowledgeTableModel) {
        JButton btnClear = new JButton("Clear facts");
        btnClear.setBounds(20, 100, 100, 40);
        panel.add(btnClear);

        btnClear.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                knowledgeTableModel.clearFactsListStates();
            }
        });
    }

    private void initOptions(JFrame mainWindow, final KnowledgeTableModel knowledgeTableModel,
                             final QueueManager queueManager) {
        JPanel panel = new JPanel();
        panel.setLayout(null);

        JLabel lblTipEpsilon = new JLabel("max tip size: ");
        lblTipEpsilon.setBounds(20, 20, 100, 20);
        panel.add(lblTipEpsilon);

        SpinnerModel spinnerModel = new SpinnerNumberModel(queueManager.getTipEpsilon(), 1, 9, 1);
        JSpinner spinner = new JSpinner(spinnerModel);
        JSpinner.DefaultEditor editor = (JSpinner.DefaultEditor) spinner.getEditor();
        editor.getTextField().setEditable(false);
        spinner.setBounds(130, 20, 50, 20);
        spinner.addChangeListener(new ChangeListener() {
            public void stateChanged(ChangeEvent e) {
                JSpinner spinner = (JSpinner)e.getSource();
                queueManager.setTipEpsilon(Integer.parseInt(spinner.getValue().toString()));
            }
        });
        panel.add(spinner);

        JLabel lblWantedResult = new JLabel("wanted result: ");
        lblWantedResult.setBounds(20, 50, 100, 20);
        panel.add(lblWantedResult);

        JComboBox possibleResultsList = new JComboBox(Utils.WANTED_RESULT_OPTIONS);
        possibleResultsList.setBounds(130, 50, 130, 20);
        possibleResultsList.setSelectedItem(queueManager.getWantedResult());
        possibleResultsList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox comboBox = (JComboBox)e.getSource();
                String wantedResult = (String)comboBox.getSelectedItem();
                queueManager.setWantedResult(wantedResult);
            }
        });
        panel.add(possibleResultsList);

        add(panel, BorderLayout.CENTER);

        initClearButton(panel, knowledgeTableModel);
        initStartButton(mainWindow, panel, knowledgeTableModel, queueManager);
    }
}