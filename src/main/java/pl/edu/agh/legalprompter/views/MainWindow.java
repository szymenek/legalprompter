package pl.edu.agh.legalprompter.views;


import pl.edu.agh.legalprompter.core.QueueManager;
import pl.edu.agh.legalprompter.views.helpers.argumentation.ArgumentationTreeCreator;
import pl.edu.agh.legalprompter.views.helpers.argumentation.TipArgumentationTreeCreator;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.*;
import java.net.URL;

public class MainWindow extends JFrame {

    private QueueManager queueManager;

    private JPanel rootPanel;
    private JPanel knowledgePanel;
    private JTree treePrecedences;
    private JTree treeStatutes;
    private JTree treeTips;
    private DefaultMutableTreeNode rootPrecedences;
    private DefaultMutableTreeNode rootStatutes;
    private DefaultMutableTreeNode rootTips;

    public MainWindow(QueueManager queueManager) {
        super("LegalPrompter");

        this.queueManager = queueManager;

        loadIcon("icon.gif");
        setContentPane(rootPanel);
        pack();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    private void loadIcon(String strPath)
    {
        URL imgUrl = getClass().getClassLoader().getResource(strPath);
        if(imgUrl != null)
            setIconImage(new ImageIcon(imgUrl).getImage());
    }

    private void createUIComponents() {
        knowledgePanel = new KnowledgePanel(this, queueManager);

        rootPrecedences = new DefaultMutableTreeNode("Found precedents");
        rootStatutes = new DefaultMutableTreeNode("Found statutes");
        rootTips = new DefaultMutableTreeNode("Tips");

        treePrecedences = new JTree(new DefaultTreeModel(rootPrecedences));
        treeStatutes = new JTree(new DefaultTreeModel(rootStatutes));
        treeTips = new JTree(new DefaultTreeModel(rootTips));

        final Font font = new Font("Arial", Font.PLAIN, 14);
        treePrecedences.setFont(font);
        treeStatutes.setFont(font);
        treeTips.setFont(font);

        updateGuiResults();
    }

    public void updateGuiResults() {
        rootPrecedences.removeAllChildren();
        rootStatutes.removeAllChildren();
        rootTips.removeAllChildren();

        new ArgumentationTreeCreator(treePrecedences, rootPrecedences, queueManager.getResultPrecedences())
                .create();
        new ArgumentationTreeCreator(treeStatutes, rootStatutes, queueManager.getResultStatutes())
                .create();
        new TipArgumentationTreeCreator(treeTips, rootTips, queueManager.getTipFormulas(), queueManager.getResultTips()).create();
    }
}