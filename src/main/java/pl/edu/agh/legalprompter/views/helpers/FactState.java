package pl.edu.agh.legalprompter.views.helpers;


public enum FactState {
    False, True, Unknown;
}
