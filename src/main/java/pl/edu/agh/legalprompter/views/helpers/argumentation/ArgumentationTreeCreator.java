package pl.edu.agh.legalprompter.views.helpers.argumentation;


import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.models.LegalFormula;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.Collections;
import java.util.List;

public class ArgumentationTreeCreator {

    protected JTree tree;
    protected DefaultMutableTreeNode root;
    protected List<LegalFormula> formulas;

    protected ArgumentationTreeCreator() {

    }

    public ArgumentationTreeCreator(JTree tree, DefaultMutableTreeNode root, List<LegalFormula> formulas) {
        this.tree = tree;
        this.root = root;
        this.formulas = formulas;
        Collections.sort(this.formulas);
    }

    public void create() {
        createNode();
        ((DefaultTreeModel)tree.getModel()).reload();
    }

    protected void createNode() {
        for(LegalFormula formula : formulas) {
            DefaultMutableTreeNode conclusion = new DefaultMutableTreeNode(formula);
            root.add(conclusion);

            createNodeArgumentation(conclusion, formula.getPremises());
        }
    }

    private void createNodeArgumentation(DefaultMutableTreeNode node, List<LegalFact> facts) {
        for(LegalFact fact : facts) {
            DefaultMutableTreeNode premise = new DefaultMutableTreeNode(fact);
            node.add(premise);
            if(fact.getPremises() != null && fact.getPremises().size() > 0)
                createNodeArgumentation(premise, fact.getPremises().get(0));
        }
    }
}
