package pl.edu.agh.legalprompter.agents;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import pl.edu.agh.legalprompter.core.Blackboard;
import pl.edu.agh.legalprompter.models.LegalFormula;
import pl.edu.agh.legalprompter.utils.LegalJsonImporter;

public abstract class AbstractAgent {

	protected final String SOURCE_FILE;

	protected Blackboard blackboard;
	protected List<LegalFormula> knowledge;
	
	public AbstractAgent(String sourceFile) {
		SOURCE_FILE = sourceFile;

		this.knowledge = new ArrayList<LegalFormula>();
		this.blackboard = new Blackboard();
	}

	public void setBlackboard(Blackboard blackboard) {
		this.blackboard = blackboard;
	}

	public Blackboard getBlackboard() {
		return blackboard;
	}

	public List<LegalFormula> getKnowledge() {
		return knowledge;
	}

	public void setKnowledge(List<LegalFormula> formulas) {
		knowledge = formulas;
	}

	protected void acquireKnowledge(Type type) {
		BufferedReader br = null;
		try {
			File file = new File("LP_knowledge/" + SOURCE_FILE);
			if(!file.exists()) {
				file.getParentFile().mkdir();
				OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file));
				InputStream inputStream = getClass().getClassLoader().getResourceAsStream(SOURCE_FILE);
				int in;
				while (-1 != (in = inputStream.read())){
					outputStream.write((byte) in);
				}
				outputStream.flush();
				outputStream.close();
			}
			br = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		knowledge = LegalJsonImporter.getInstance().fromJson(br, type);
		try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void dumpKnowledge() {
		try {
			File file = new File("LP_knowledge/" + SOURCE_FILE);
			file.getParentFile().mkdir();
			FileWriter fw = new FileWriter(file, false);
			fw.write(LegalJsonImporter.getInstance().toJson(knowledge));
			fw.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public abstract void acquireKnowledge();
}
