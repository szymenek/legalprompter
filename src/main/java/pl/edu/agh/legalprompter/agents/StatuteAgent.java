package pl.edu.agh.legalprompter.agents;

import com.google.gson.reflect.TypeToken;
import pl.edu.agh.legalprompter.models.DescribedLegalFormula;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class StatuteAgent extends AbstractAgent {

	public StatuteAgent(String sourceFile) {
		super(sourceFile);
	}

	@Override
	public void acquireKnowledge() {
		Type type = new TypeToken<ArrayList<DescribedLegalFormula>>(){}.getType();
		super.acquireKnowledge(type);
	}
}
