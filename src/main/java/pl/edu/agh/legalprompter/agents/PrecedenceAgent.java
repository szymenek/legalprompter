package pl.edu.agh.legalprompter.agents;

import com.google.gson.reflect.TypeToken;
import pl.edu.agh.legalprompter.models.PrecedenceLegalFormula;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class PrecedenceAgent extends AbstractAgent {

	public PrecedenceAgent(String sourceFile) {
		super(sourceFile);
	}

	@Override
	public void acquireKnowledge() {
		Type type = new TypeToken<ArrayList<PrecedenceLegalFormula>>(){}.getType();
		super.acquireKnowledge(type);
	}
}
