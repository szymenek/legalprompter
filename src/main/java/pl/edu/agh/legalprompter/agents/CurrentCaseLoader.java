package pl.edu.agh.legalprompter.agents;


import com.google.gson.reflect.TypeToken;
import pl.edu.agh.legalprompter.models.LegalFormula;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class CurrentCaseLoader extends AbstractAgent {

    public CurrentCaseLoader(String sourceFile) {
        super(sourceFile);
    }

    @Override
    public void acquireKnowledge() {
        Type type = new TypeToken<ArrayList<LegalFormula>>(){}.getType();
        super.acquireKnowledge(type);
    }
}
