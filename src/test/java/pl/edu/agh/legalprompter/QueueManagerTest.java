package pl.edu.agh.legalprompter;


import org.junit.Test;
import pl.edu.agh.legalprompter.agents.*;
import pl.edu.agh.legalprompter.core.Blackboard;
import pl.edu.agh.legalprompter.core.QueueManager;
import pl.edu.agh.legalprompter.models.DescribedLegalFormula;
import pl.edu.agh.legalprompter.models.LegalFormula;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class QueueManagerTest {

    private PrecedenceAgent precedenceAgent = new PrecedenceAgent("test_precedences.json");
    private StatuteAgent statuteAgent = new StatuteAgent("test_statutes.json");
    private CommonKnowledgeAgent commonKnowledgeAgent = new CommonKnowledgeAgent("test_common_knowledge.json");
    private GeneralLawAgent generalLawAgent = new GeneralLawAgent("test_general_law.json");

    private QueueManager createQueueManager(String currentCaseSourceFile) {
        QueueManager queueManager = new QueueManager(new Blackboard(), currentCaseSourceFile);
        queueManager.addAgent(precedenceAgent);
        queueManager.addAgent(statuteAgent);
        queueManager.addAgent(commonKnowledgeAgent);
        queueManager.addAgent(generalLawAgent);
        return  queueManager;
    }

    public static Set<LegalFormula> findFormulas(List<LegalFormula> formulas, List<String> signatures) {
        Set<LegalFormula> result = new HashSet<LegalFormula>();
        for(LegalFormula legalFormula : formulas) {
            if(signatures.contains(((DescribedLegalFormula)legalFormula).getSignature())) {
                result.add(legalFormula);
            }
        }
        return result;
    }

    @Test
    public void testExample1() {
        QueueManager queueManager = createQueueManager("test_current_case1.json");
        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<LegalFormula> precedences = queueManager.getResultPrecedences();
        List<LegalFormula> statutes = queueManager.getResultStatutes();
        List<LegalFormula> tips = queueManager.getTipFormulas();

        assertEquals("Incorrect number of precedences found", 1, precedences.size());
        assertEquals("Incorrect number of statutes found", 1, statutes.size());
        assertEquals("Incorrect number of tips found", 8, tips.size());

        List<String> signatures = new ArrayList<String>();
        signatures.add("XI W 10373/14");
        Set<LegalFormula> formulas = findFormulas(precedenceAgent.getKnowledge(), signatures);
        assertEquals("Incorrect precedences found", formulas, new HashSet<LegalFormula>(precedences));

        signatures.clear();
        signatures.add("art. 156 § 1 k.k.");
        formulas = findFormulas(statuteAgent.getKnowledge(), signatures);
        assertTrue("Incorrect statutes found", formulas.containsAll(new HashSet<LegalFormula>(statutes)));

        signatures.clear();
        signatures.add("VII W 644/14");
        signatures.add("VI Ka 141/13");
        signatures.add("XVII Ka 341/15");
        signatures.add("II W 1724/15");
        signatures.add("IV Ka 163/15");
        signatures.add("IV Ka 445/15");
        signatures.add("IV Ka 127/15");
        signatures.add("art. 156 § 3 k.k.");
        formulas = findFormulas(precedenceAgent.getKnowledge(), signatures);
        formulas.addAll(findFormulas(statuteAgent.getKnowledge(), signatures));
        assertTrue("Incorrect tips found", formulas.containsAll(new HashSet<LegalFormula>(tips)));
    }

    @Test
    public void testExample2() {
        QueueManager queueManager = createQueueManager("test_current_case2.json");
        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<LegalFormula> precedences = queueManager.getResultPrecedences();
        List<LegalFormula> statutes = queueManager.getResultStatutes();
        List<LegalFormula> tips = queueManager.getTipFormulas();

        assertEquals("Incorrect number of precedences found", 4, precedences.size());
        assertEquals("Incorrect number of statutes found", 2, statutes.size());
        assertEquals("Incorrect number of tips found", 7, tips.size());

        List<String> signatures = new ArrayList<String>();
        signatures.add("XI W 10373/14");
        signatures.add("XVII Ka 341/15");
        signatures.add("VI Ka 101/13");
        signatures.add("VI Ka 132/15");
        Set<LegalFormula> formulas = findFormulas(precedenceAgent.getKnowledge(), signatures);
        assertEquals("Incorrect precedences found", formulas, new HashSet<LegalFormula>(precedences));

        signatures.clear();
        signatures.add("art. 156 § 2 k.k.");
        signatures.add("art. 177 § 2 k.k.");
        formulas = findFormulas(statuteAgent.getKnowledge(), signatures);
        assertTrue("Incorrect statutes found", formulas.containsAll(new HashSet<LegalFormula>(statutes)));

        signatures.clear();
        signatures.add("IV Ka 939/13");
        signatures.add("VI Ka 141/13");
        signatures.add("II W 1724/15");
        signatures.add("IV Ka 445/15");
        signatures.add("IV Ka 127/15");
        signatures.add("Signature 4");
        signatures.add("Signature 3");
        formulas = findFormulas(precedenceAgent.getKnowledge(), signatures);
        formulas.addAll(findFormulas(statuteAgent.getKnowledge(), signatures));
        assertTrue("Incorrect tips found", formulas.containsAll(new HashSet<LegalFormula>(tips)));
    }
}
