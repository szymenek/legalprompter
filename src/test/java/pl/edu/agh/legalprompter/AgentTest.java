package pl.edu.agh.legalprompter;


import org.junit.Test;
import pl.edu.agh.legalprompter.agents.*;
import pl.edu.agh.legalprompter.core.Blackboard;
import pl.edu.agh.legalprompter.core.QueueManager;
import pl.edu.agh.legalprompter.models.LegalFact;
import pl.edu.agh.legalprompter.models.LegalFormula;

import java.util.*;

import static org.junit.Assert.*;

public class AgentTest {

    private QueueManager createQueueManager(String currentCaseSourceFile, AbstractAgent agent) {
        QueueManager queueManager = new QueueManager(new Blackboard(), currentCaseSourceFile);
        queueManager.addAgent(agent);
        return  queueManager;
    }

    @Test
    public void testCommonKnowledgeAgent() {
        CommonKnowledgeAgent commonKnowledgeAgent = new CommonKnowledgeAgent("test_common_knowledge.json");
        QueueManager queueManager = createQueueManager("test_current_case1.json", commonKnowledgeAgent);
        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<Object> knowledge = new ArrayList<Object>(queueManager.getBlackboard().getSession().getObjects());
        List<LegalFact> currentCaseFacts = queueManager.getCurrentCaseFacts();

        assertEquals("Incorrect facts amount in working memory", 16, knowledge.size()); //agent is inside
        assertEquals("Incorrect facts amount in current case", 10, currentCaseFacts.size());

        String[] factNames = new String[] {"you_hit_other_road_user", "you_hit_a_vehicle", "you_hit_a_pedestrian",
                "you_hit_a_motor", "you_hit_a_bike"} ;

        for(String factName : factNames) {
            LegalFact legalFact = new LegalFact(factName);
            assertTrue("Fact "+legalFact+" should be in knowledge", knowledge.contains(legalFact));
            assertFalse("Fact "+legalFact+" should not be in knowledge", currentCaseFacts.contains(legalFact));
        }
    }

    @Test
    public void testGeneralLawAgent() {
        GeneralLawAgent generalLawAgent = new GeneralLawAgent("test_general_law.json");
        QueueManager queueManager = createQueueManager("test_current_case1.json", generalLawAgent);
        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<Object> knowledge = new ArrayList<Object>(queueManager.getBlackboard().getSession().getObjects());
        List<LegalFact> currentCaseFacts = queueManager.getCurrentCaseFacts();

        assertEquals("Incorrect facts amount in working memory", 12, knowledge.size()); //agent is inside
        assertEquals("Incorrect facts amount in current case", 10, currentCaseFacts.size());

        LegalFact legalFact = new LegalFact("!you_take_special_care");
        assertTrue("Fact "+legalFact+" should be in knowledge", knowledge.contains(legalFact));
        assertFalse("Fact "+legalFact+" should not be in knowledge", currentCaseFacts.contains(legalFact));
    }

    @Test
    public void testPrecedenceAgent() {
        PrecedenceAgent precedenceAgent = new PrecedenceAgent("test_precedences.json");
        QueueManager queueManager = createQueueManager("test_current_case3.json", precedenceAgent);

        assertEquals("Incorrect precedences amount found before analysis",
                0, queueManager.getResultPrecedences().size());

        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<Object> knowledge = new ArrayList<Object>(queueManager.getBlackboard().getSession().getObjects());
        List<LegalFact> currentCaseFacts = queueManager.getCurrentCaseFacts();
        List<LegalFormula> precedences = queueManager.getResultPrecedences();

        assertEquals("Incorrect precedences amount found after analysis", 1, precedences.size());
        assertEquals("Incorrect facts amount in working memory", 12, knowledge.size()); //agent is inside, 2 facts added
        assertEquals("Incorrect facts amount in current case", 9, currentCaseFacts.size());

        List<String> signatures = new ArrayList<String>();
        signatures.add("XI W 10373/14");
        Set<LegalFormula> formulas = QueueManagerTest.findFormulas(precedenceAgent.getKnowledge(), signatures);
        assertEquals("Incorrect precedences found", formulas, new HashSet<LegalFormula>(precedences));
    }

    @Test
    public void testStatuteAgent() {
        StatuteAgent statuteAgent = new StatuteAgent("test_statutes.json");
        QueueManager queueManager = createQueueManager("test_current_case1.json", statuteAgent);

        assertEquals("Incorrect statutes amount found before analysis", 0, queueManager.getResultStatutes().size());

        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        assertEquals("Incorrect statutes amount found after analysis", 1, queueManager.getResultStatutes().size());

        List<Object> knowledge = new ArrayList<Object>(queueManager.getBlackboard().getSession().getObjects());
        List<LegalFact> currentCaseFacts = queueManager.getCurrentCaseFacts();

        assertEquals("Incorrect facts amount in working memory", 12, knowledge.size()); //agent is inside
        assertEquals("Incorrect facts amount in current case", 10, currentCaseFacts.size());

        LegalFact legalFact = new LegalFact("imprisoned_[12..120]_months");
        assertTrue("Fact "+legalFact+" should be in knowledge", knowledge.contains(legalFact));
        assertFalse("Fact "+legalFact+" should not be in knowledge", currentCaseFacts.contains(legalFact));
    }

    @Test
    public void testTipPrecedenceAgent() {
        PrecedenceAgent tipPrecedenceAgent = new PrecedenceAgent("test_precedences.json");
        QueueManager queueManager = createQueueManager("test_current_case1.json", tipPrecedenceAgent);

        assertEquals("Incorrect tips (precedences) amount found before analysis",
                0, queueManager.getTipFormulas().size());
        assertEquals("Incorrect unknown premises for tips (precedences) amount found after analysis",
                0, queueManager.getResultTipUnknownPremises().size());

        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<LegalFormula> tips = queueManager.getTipFormulas();
        List<List<LegalFact>> tipUnknownPremises = queueManager.getResultTipUnknownPremises();

        assertEquals("Incorrect tips (precedences) amount found after analysis", 5, tips.size());
        assertEquals("Incorrect tips (precedences) amount found after analysis", 5, tipUnknownPremises.size());

        List<String> signatures = new ArrayList<String>();
        signatures.add("VII W 644/14");
        signatures.add("VI Ka 141/13");
        signatures.add("XI W 10373/14");
        signatures.add("II W 1724/15");
        signatures.add("IV Ka 127/15");
        Set<LegalFormula> formulas = QueueManagerTest.findFormulas(tipPrecedenceAgent.getKnowledge(), signatures);
        assertEquals("Incorrect tips (precedences) found", formulas, new HashSet<LegalFormula>(tips));

        Set<LegalFact> expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("!you_take_special_care"));
        assertEquals("Incorrect unknown premises for tip 1 (precedences)",
                expectedPremises, new HashSet<LegalFact>(tipUnknownPremises.get(0)));
        assertEquals("Incorrect rank for tip 1 (precedences)", 1, tips.get(0).getRank());

        expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("!there_were_casualties"));
        expectedPremises.add(new LegalFact("you_were_on_the_unprivileged_road"));
        assertEquals("Incorrect unknown premises for tip 2 (precedences)",
                expectedPremises, new HashSet<LegalFact>(tipUnknownPremises.get(1)));
        assertEquals("Incorrect rank for tip 2 (precedences)", 2, tips.get(1).getRank());

        expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("accident_at_the_crossroads"));
        expectedPremises.add(new LegalFact("high_speed"));
        assertEquals("Incorrect unknown premises for tip 3 (precedences)",
                expectedPremises, new HashSet<LegalFact>(tipUnknownPremises.get(2)));
        assertEquals("Incorrect rank for tip 3 (precedences)", 2, tips.get(2).getRank());

        expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("illegal_maneuver"));
        expectedPremises.add(new LegalFact("head-on_crash"));
        assertEquals("Incorrect unknown premises for tip 4 (precedences)",
                expectedPremises, new HashSet<LegalFact>(tipUnknownPremises.get(3)));
        assertEquals("Incorrect rank for tip 4 (precedences)", 2, tips.get(3).getRank());

        expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("!you_take_special_care"));
        expectedPremises.add(new LegalFact("punished_for_similar_offenses"));
        assertEquals("Incorrect unknown premises for tip 5 (precedences)",
                expectedPremises, new HashSet<LegalFact>(tipUnknownPremises.get(4)));
        assertEquals("Incorrect rank for tip 5 (precedences)", 2, tips.get(4).getRank());
    }

    @Test
    public void testTipStatuteAgent() {
        StatuteAgent tipStatuteAgent = new StatuteAgent("test_statutes.json");
        QueueManager queueManager = createQueueManager("test_current_case1.json", tipStatuteAgent);

        assertEquals("Incorrect tips (statutes) amount found before analysis",
                0, queueManager.getTipFormulas().size());
        assertEquals("Incorrect unknown premises for tips (statutes) amount found after analysis",
                0, queueManager.getResultTipUnknownPremises().size());

        queueManager.acquireKnowledge();
        queueManager.startLegalAnalysis();

        List<LegalFormula> tips = queueManager.getTipFormulas();
        List<List<LegalFact>> tipUnknownPremises = queueManager.getResultTipUnknownPremises();

        assertEquals("Incorrect tips (statutes) amount found after analysis", 1, tips.size());
        assertEquals("Incorrect tips (statutes) amount found after analysis", 1, tipUnknownPremises.size());

        List<String> signatures = new ArrayList<String>();
        signatures.add("art. 156 § 3 k.k.");
        Set<LegalFormula> formulas = QueueManagerTest.findFormulas(tipStatuteAgent.getKnowledge(), signatures);
        assertTrue("Incorrect tips (statutes) found", formulas.containsAll(new HashSet<LegalFormula>(tips)));

        Set<LegalFact> expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("there_were_casualties"));
        assertEquals("Incorrect unknown premises for tip (statutes)",
                expectedPremises, new HashSet<LegalFact>(tipUnknownPremises.get(0)));
    }

    @Test
    public void testCurrentCaseLoader() {
        CurrentCaseLoader currentCaseLoader = new CurrentCaseLoader("test_current_case1.json");
        currentCaseLoader.acquireKnowledge();
        LegalFormula legalFormula = currentCaseLoader.getKnowledge().get(0);

        assertNotNull("LegalFormula of current case is null.", legalFormula);

        Set<LegalFact> expectedPremises = new HashSet<LegalFact>();
        expectedPremises.add(new LegalFact("!you_give_a_way"));
        expectedPremises.add(new LegalFact("!you_look_at_signs"));
        expectedPremises.add(new LegalFact("there_were_injuries"));
        expectedPremises.add(new LegalFact("!you_were_drunk"));
        expectedPremises.add(new LegalFact("you_hit_a_car"));
        expectedPremises.add(new LegalFact("intentional"));
        expectedPremises.add(new LegalFact("injured_more_than_7_days"));
        expectedPremises.add(new LegalFact("heavy_damage_on_health"));
        expectedPremises.add(new LegalFact("!showed_necessary_documents"));
        expectedPremises.add(new LegalFact("run_away_from_accident_place"));
        assertEquals("Incorrect premises of current case",
                expectedPremises, new HashSet<LegalFact>(legalFormula.getPremises()));

        List<LegalFact> expectedConclusions = new ArrayList<LegalFact>();
        expectedConclusions.add(new LegalFact(""));
        assertEquals("Incorrect conclusions of current case",
                expectedConclusions, legalFormula.getConclusions());
    }
}
